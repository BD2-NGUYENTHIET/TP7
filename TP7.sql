set serveroutput on;

-- Donne les droits
grant select on village to c##rlemont_a;
grant insert on village to c##rlemont_a;

select * from c##rlemont_a.Villages;

declare 
    le_nb int;
begin
    traitement3(50,le_nb);
    dbms_output.put_line(le_nb);
end;
/

grant execute on traitement3 to c##rlemont_a;

-- Exo2
-- Unique outil : grant sur table ( pas sur les procédures pour l'instant )

-- 1
-- grant insert on village to e1,e2,...,en 
-- (tout pile assez de droit)

-- 2
-- grant select on village to e1,e2,...,en
-- (tout pile assez de droit)

-- 3
-- grant update(capacite,activite) on village to e1,e2,...,en
-- (tout pile assez de droit)

-- 4
-- grant sejour on village to e1,e2,...,en
-- (tout pile assez de droit)

-- 5
-- grant delete,select on sejour to e1,e2,...,en
-- permet des choses interdites : effacer quelques sejours inférieur à une date donné

-- 6
-- grant insert on client to c1,c2,...,cn
-- on peut mettre nimp en avoir 

-- 7
-- grant select on village to c1,c2,...,cn
-- grant update on client to c1,c2,...,cn
-- grant insert on sejour to c1,c2,...,cn
-- on peut insérer des sejours sans payer

-- 8
-- grant select on village,sejour to c1,c2,...,cn



-- grant select on client,village,sejour to c1,c2,...,cn

-- EX3

-- Step:
-- remise à zéro
--  recup droit ds table sys
--  revoke ces droits
--  (revoke all from all to all)
-- bon droitsproc
-- Outils : grant sur tables, vues, proc

action 1 :
    solution d avant est ok

action 2 :
    ok

action 3 :
    ok

action 4 :
    ok

action 5 :
    create proc traitement3
    grant execute on traitement3 to e1...en

action 6 :
    create proc traitement1
    grant execute on traitement1 to e1...en

action 7 :
    create proc traitement2
    grant execute on traitement2 to e1...en

action 8 :
    create vue_village_sans_sejour as select ...
    grant select on vue_village_sans_sejour

action 9 :
    create fonction...
        consulter_village(idc) : ...
        consulter_client(idc) : ...
        consulter_sejour(idc) : ...

    grant execute on fonction